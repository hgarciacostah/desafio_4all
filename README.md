# 4All_Desafio

Desafio para oportunidade QA. - Henrique Garcia Costa






**ESTRUTURA AUTOMAÇÃO**

A estrutura escolhida segue o padrão abaixo:

**1 - Classes PageObjects:** Este tipo de classe é responsavel por armazenar todos os “Locators” e funções de uma Web Page especifica;

**2 - Classe De testes:** Responsavel por armazenar as funções, inputs e funções auxiliares (ex: função “ScreenShot) que representam os passos do caso de teste;

**4 - Testes Paralelos** - classe responsavel por rodar os testes na ordem desejada;

**5 - Classe Auxiliar** - Responsavel por armazenar funções úteis como: “Definir caminho para o chrome.exe” e “ScreenShot”

**VIDEO 1 :** Segue video abaixo onde mostro a estrutura:
https://www.loom.com/share/9671061ae52848bf962febd2bf4d870c




**RODANDO OS TESTES + EVIDÊNCIAS + TEST SUITE**


**VÍDEO 2 :** Por favor, veja o vídeo abaixo onde os dois testes são rodados através de um Test Suite e as evidências capturadas:

https://www.loom.com/share/f58fa255a3954cf9a9a9eb7b830aeff8


**ESCOLHENDO O BROWSER A SER UTILIZADO VIA PARAMETRO**


Acesse a classe: "FuncoesAuxiliares" na função: 'DefinirBrowser' defina o caminho onde seus drivers.exe estao salvos através da variavel de função 'driverPath'. 
Esta função deve ser utilizada nas classes de "Testes" e o paramentro "browser" deve ser preechido com "chrome" ou "firefox" no formato string. Por faor veja o video abaixo onde mostro os dois testes rodando em browsers diferentes respeitando o parametro da função:

**VIDEO 3**

https://www.loom.com/share/8c322f3609a8421eb91636b4e95e3384


**FUNÇÃO SREENSHOT**

1 - Definindo o caminho para as evidencias:
     Para isto, acesse a classe "FuncoesAuxiliares" e a função 'screenshot';
2 - Essa função é composta pelos parametros (Driver, string caminhoParaEvidencias);
3 - Seu uso, em uma classe de testes, seria assim:
     fa.screenShot(driver, "**caminho para guardar as evidencias** + **nome da evidencia**.png");
4 - Devido a minha escolha em determinar os nomes das evidencias como o nome do passo sendo executado, é preciso repetir os parametros durante a utilização da função, deste jeito:

            // selecionar categoria DOCES
            fa.screenShot(driver, "_C:/Nova pasta/EvidenciasDesafio1/_**1_SelecionaCategoriaDoces**.png");

            // Adicionar todos o produtos de Doces no carrinho  
            fa.screenShot(driver, "_C:/Nova pasta/EvidenciasDesafio1/_**2_AdicionarTodosProdutos**.png");

            // Clica na categoria todos
            fa.screenShot(driver, "_C:/Nova pasta/EvidenciasDesafio1/_**3_SelecionaCategoriaTodos**.png");
            


**POSSIVEIS PROBLEMAS PARA SE RODA O PROJETO:**

1- É preciso indicar o caminho na sua máquina onde os drivers estao instalados. Por favor, faça isto na Classe “FuncoesAuxiliares” e nao função “DefinirCaminhoPara_Chrome_exe()”;







